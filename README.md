# Openpiton_Ariane_zcu102
The repo contains all the projects created.

Directory Description:

-ariane_zcu102
	
	-cva6
	Single core Ariane project for zcu102 for debugging.

	-cva6_zcu102	
	Single core Ariane project for zcu102
	
	-opensbi_zcu102
	opensbi project modified for zcu102.
	
	-tii-zcu-guide_zcu102
	Tii guide modified for zcu102

	-zcu102_bd.tcl
	Block design for zcu102.
		
-openpiton_ariane_zcu102

	-openpiton_zcu102.tar.gz
	Synthesized and implemented vivado project for genesys2 board: build/genesys2
		* synthezied and implemented vivado project for vc707 board: build/vc707
		* synthesized vivado project for zcu102 board: build/vc707/vc707_system.xpr
		* built binaries and results for simulation of openpiton_ariane with 4 cores: build/fake_uart.log

