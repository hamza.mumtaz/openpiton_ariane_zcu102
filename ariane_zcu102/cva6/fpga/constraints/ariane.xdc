## Common Ariane XDCs

#create_clock -period 100.000 -name tck -waveform {0.000 50.000} [get_ports tck]
#set_input_jitter tck 1.000

# minimize routing delay
#set_input_delay  -clock tck -clock_fall 5 [get_ports tdi    ]
#set_input_delay  -clock tck -clock_fall 5 [get_ports tms    ]
#set_output_delay -clock tck             5 [get_ports tdo    ]
#set_false_path   -from                    [get_ports trst_n ]


set_max_delay -datapath_only -from [get_pins i_dmi_jtag/i_dmi_cdc/i_cdc_resp/i_src/data_src_q_reg*/C] -to [get_pins i_dmi_jtag/i_dmi_cdc/i_cdc_resp/i_dst/data_dst_q_reg*/D] 20.000
set_max_delay -datapath_only -from [get_pins i_dmi_jtag/i_dmi_cdc/i_cdc_resp/i_src/req_src_q_reg/C] -to [get_pins i_dmi_jtag/i_dmi_cdc/i_cdc_resp/i_dst/req_dst_q_reg/D] 20.000
set_max_delay -datapath_only -from [get_pins i_dmi_jtag/i_dmi_cdc/i_cdc_req/i_dst/ack_dst_q_reg/C] -to [get_pins i_dmi_jtag/i_dmi_cdc/i_cdc_req/i_src/ack_src_q_reg/D] 20.000

# set multicycle path on reset, on the FPGA we do not care about the reset anyway
set_multicycle_path -from [get_pins i_rstgen_main/i_rstgen_bypass/synch_regs_q_reg[3]/C] 4
set_multicycle_path -from [get_pins i_rstgen_main/i_rstgen_bypass/synch_regs_q_reg[3]/C] 3  -hold

create_debug_core u_ila_0 ila
set_property ALL_PROBE_SAME_MU true [get_debug_cores u_ila_0]
set_property ALL_PROBE_SAME_MU_CNT 1 [get_debug_cores u_ila_0]
set_property C_ADV_TRIGGER false [get_debug_cores u_ila_0]
set_property C_DATA_DEPTH 1024 [get_debug_cores u_ila_0]
set_property C_EN_STRG_QUAL false [get_debug_cores u_ila_0]
set_property C_INPUT_PIPE_STAGES 0 [get_debug_cores u_ila_0]
set_property C_TRIGIN_EN false [get_debug_cores u_ila_0]
set_property C_TRIGOUT_EN false [get_debug_cores u_ila_0]
set_property port_width 1 [get_debug_ports u_ila_0/clk]
connect_debug_port u_ila_0/clk [get_nets [list zcu_102_ps_o/zcu102_ps_wrapper_i/zynq_ultra_ps_e_0/inst/pl_clk0]]
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe0]
set_property port_width 64 [get_debug_ports u_ila_0/probe0]
connect_debug_port u_ila_0/probe0 [get_nets [list {i_ariane/i_frontend/npc_d[0]} {i_ariane/i_frontend/npc_d[1]} {i_ariane/i_frontend/npc_d[2]} {i_ariane/i_frontend/npc_d[3]} {i_ariane/i_frontend/npc_d[4]} {i_ariane/i_frontend/npc_d[5]} {i_ariane/i_frontend/npc_d[6]} {i_ariane/i_frontend/npc_d[7]} {i_ariane/i_frontend/npc_d[8]} {i_ariane/i_frontend/npc_d[9]} {i_ariane/i_frontend/npc_d[10]} {i_ariane/i_frontend/npc_d[11]} {i_ariane/i_frontend/npc_d[12]} {i_ariane/i_frontend/npc_d[13]} {i_ariane/i_frontend/npc_d[14]} {i_ariane/i_frontend/npc_d[15]} {i_ariane/i_frontend/npc_d[16]} {i_ariane/i_frontend/npc_d[17]} {i_ariane/i_frontend/npc_d[18]} {i_ariane/i_frontend/npc_d[19]} {i_ariane/i_frontend/npc_d[20]} {i_ariane/i_frontend/npc_d[21]} {i_ariane/i_frontend/npc_d[22]} {i_ariane/i_frontend/npc_d[23]} {i_ariane/i_frontend/npc_d[24]} {i_ariane/i_frontend/npc_d[25]} {i_ariane/i_frontend/npc_d[26]} {i_ariane/i_frontend/npc_d[27]} {i_ariane/i_frontend/npc_d[28]} {i_ariane/i_frontend/npc_d[29]} {i_ariane/i_frontend/npc_d[30]} {i_ariane/i_frontend/npc_d[31]} {i_ariane/i_frontend/npc_d[32]} {i_ariane/i_frontend/npc_d[33]} {i_ariane/i_frontend/npc_d[34]} {i_ariane/i_frontend/npc_d[35]} {i_ariane/i_frontend/npc_d[36]} {i_ariane/i_frontend/npc_d[37]} {i_ariane/i_frontend/npc_d[38]} {i_ariane/i_frontend/npc_d[39]} {i_ariane/i_frontend/npc_d[40]} {i_ariane/i_frontend/npc_d[41]} {i_ariane/i_frontend/npc_d[42]} {i_ariane/i_frontend/npc_d[43]} {i_ariane/i_frontend/npc_d[44]} {i_ariane/i_frontend/npc_d[45]} {i_ariane/i_frontend/npc_d[46]} {i_ariane/i_frontend/npc_d[47]} {i_ariane/i_frontend/npc_d[48]} {i_ariane/i_frontend/npc_d[49]} {i_ariane/i_frontend/npc_d[50]} {i_ariane/i_frontend/npc_d[51]} {i_ariane/i_frontend/npc_d[52]} {i_ariane/i_frontend/npc_d[53]} {i_ariane/i_frontend/npc_d[54]} {i_ariane/i_frontend/npc_d[55]} {i_ariane/i_frontend/npc_d[56]} {i_ariane/i_frontend/npc_d[57]} {i_ariane/i_frontend/npc_d[58]} {i_ariane/i_frontend/npc_d[59]} {i_ariane/i_frontend/npc_d[60]} {i_ariane/i_frontend/npc_d[61]} {i_ariane/i_frontend/npc_d[62]} {i_ariane/i_frontend/npc_d[63]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe1]
set_property port_width 64 [get_debug_ports u_ila_0/probe1]
connect_debug_port u_ila_0/probe1 [get_nets [list {i_ariane/i_frontend/npc_q[0]} {i_ariane/i_frontend/npc_q[1]} {i_ariane/i_frontend/npc_q[2]} {i_ariane/i_frontend/npc_q[3]} {i_ariane/i_frontend/npc_q[4]} {i_ariane/i_frontend/npc_q[5]} {i_ariane/i_frontend/npc_q[6]} {i_ariane/i_frontend/npc_q[7]} {i_ariane/i_frontend/npc_q[8]} {i_ariane/i_frontend/npc_q[9]} {i_ariane/i_frontend/npc_q[10]} {i_ariane/i_frontend/npc_q[11]} {i_ariane/i_frontend/npc_q[12]} {i_ariane/i_frontend/npc_q[13]} {i_ariane/i_frontend/npc_q[14]} {i_ariane/i_frontend/npc_q[15]} {i_ariane/i_frontend/npc_q[16]} {i_ariane/i_frontend/npc_q[17]} {i_ariane/i_frontend/npc_q[18]} {i_ariane/i_frontend/npc_q[19]} {i_ariane/i_frontend/npc_q[20]} {i_ariane/i_frontend/npc_q[21]} {i_ariane/i_frontend/npc_q[22]} {i_ariane/i_frontend/npc_q[23]} {i_ariane/i_frontend/npc_q[24]} {i_ariane/i_frontend/npc_q[25]} {i_ariane/i_frontend/npc_q[26]} {i_ariane/i_frontend/npc_q[27]} {i_ariane/i_frontend/npc_q[28]} {i_ariane/i_frontend/npc_q[29]} {i_ariane/i_frontend/npc_q[30]} {i_ariane/i_frontend/npc_q[31]} {i_ariane/i_frontend/npc_q[32]} {i_ariane/i_frontend/npc_q[33]} {i_ariane/i_frontend/npc_q[34]} {i_ariane/i_frontend/npc_q[35]} {i_ariane/i_frontend/npc_q[36]} {i_ariane/i_frontend/npc_q[37]} {i_ariane/i_frontend/npc_q[38]} {i_ariane/i_frontend/npc_q[39]} {i_ariane/i_frontend/npc_q[40]} {i_ariane/i_frontend/npc_q[41]} {i_ariane/i_frontend/npc_q[42]} {i_ariane/i_frontend/npc_q[43]} {i_ariane/i_frontend/npc_q[44]} {i_ariane/i_frontend/npc_q[45]} {i_ariane/i_frontend/npc_q[46]} {i_ariane/i_frontend/npc_q[47]} {i_ariane/i_frontend/npc_q[48]} {i_ariane/i_frontend/npc_q[49]} {i_ariane/i_frontend/npc_q[50]} {i_ariane/i_frontend/npc_q[51]} {i_ariane/i_frontend/npc_q[52]} {i_ariane/i_frontend/npc_q[53]} {i_ariane/i_frontend/npc_q[54]} {i_ariane/i_frontend/npc_q[55]} {i_ariane/i_frontend/npc_q[56]} {i_ariane/i_frontend/npc_q[57]} {i_ariane/i_frontend/npc_q[58]} {i_ariane/i_frontend/npc_q[59]} {i_ariane/i_frontend/npc_q[60]} {i_ariane/i_frontend/npc_q[61]} {i_ariane/i_frontend/npc_q[62]} {i_ariane/i_frontend/npc_q[63]}]]
set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]
set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
connect_debug_port dbg_hub/clk [get_nets clk_i]
