# CVA6 Guide
This guide provides a fully step by step tutorial on how to run a single core CVA6 based system in a ZCU FPGA.
# Table of Contents
- [CVA6 Guide](#cva6-guide)
- [Table of Contents](#table-of-contents)
- [0) Prologue](#0-prologue)
- [1) Software](#1-software)
  - [1.1) Bare-Metal Application](#11-bare-metal-application)
  - [1.2) Linux](#12-linux)
  - [1.3) OpenSBI](#13-opensbi)
  - [1.4) CVA6](#14-cva6)
- [2) FPGA](#2-fpga)
  - [2.1) Program the bitstream and run the code](#21-program-the-bitstream-and-run-the-code)
  - [Used tool versions](#used-tool-versions)

# 0) Prologue

First setup ARCH and your RISC-V toolchain prefix:

`export ARCH=riscv`\
`export CROSS_COMPILE=toolchain-prefix-`\
`export RISCV=path/to/riscv-tools`

For all software use *riscv64-unknown-elf-* gcc 10.1.0 2020.08.2, from https://static.dev.sifive.com/dev-tools/freedom-tools/v2020.08/riscv64-unknown-elf-gcc-10.1.0-2020.08.2-x86_64-linux-ubuntu14.tar.gz.

Specifically for building linux, I had to use a different toolchain *riscv64-linux-* gcc version 9.3.0 2020.02-2, which I downloaded from https://toolchains.bootlin.com/downloads/releases/toolchains/riscv64/tarballs/riscv64--glibc--bleeding-edge-2020.02-2.tar.bz2.

Next, install the [RISC-V Tools](https://github.com/riscv/riscv-tools) and make sure the `RISCV` environment variable points to where your RISC-V installation is located.

Repeat these every time you start a new terminal.

Start each step from the top-level directory.

For every path starting with */path/to/* substitute it by the corresponding absolute path in your machine.

# 1) Software

## 1.1) Bare-Metal Application

First, *cd* into the bare-metal application folder:

`cd baremetal-app`

Next, compile the application for the chosen target platform.

To compile for zcu fpga run:

`make PLATFORM=cva6-fpga-zcu`

To compile for emulator run:

`make PLATFORM=cva6-emul`

## 1.2) Linux

> **_:notebook: Note:_** The following steps shall be done using the *riscv64-linux-* toolchain.
Please make sure `ARCH=riscv` and proceed.

Clone Linux and defconfig:

`git clone https://github.com/torvalds/linux.git --depth 1 --branch=v5.12 linux-5.12`\
`cd linux-5.12`\
`make defconfig`

Enable the following configs in `make menuconfig`:

- **SERIAL_XILINX_PS_UART** 
(Device Drivers -> Character devices -> Serial drivers -> Cadence (Xilinx Zynq) UART support)
- **SERIAL_XILINX_PS_UART_CONSOLE**
(Device Drivers -> Character devices -> Serial drivers -> Cadence (Xilinx Zynq) UART support -> Cadence UART Console support)
- **HVC_RISCV_SBI**
(Device Drivers -> Character devices -> RISC-V SBI console support)
- set **INITRAMFS_SOURCE** to */path/to/cva6-guide/linux/initramfs.cpio*
(General Setup -> Initial RAM filesystem and RAM disk (initramfs/initrd) support)

> **_:notebook: Note:_** You can search for symbols using the character '/' and jump directly into the option by using the numbers (e.g., 1, 2, ...)

Every time you add binaries to initramfs you need to run the following command (assuming you have uncompssed the cpio to a *initramfs* directory in the same location):

`cd initramfs && find ./ | cpio -o -H newc > ../initramfs.cpio; cd -`

and repeat the subsequent steps.

Build Linux:

`make -j$(nproc)`

> **_:notebook: Note:_** The following steps shall be done using the *riscv64-unknown-elf-* toolchain.

Build the device trees:

`cd ../linux`\
`dtc cva6-zcu104-minimal.dts > cva6-zcu104-minimal.dtb`

And build the final image by concatening the minimal bootloader, linux and device tree binaries:

`cd lloader`\
`make ARCH=rv64 IMAGE=../../linux-5.12/arch/riscv/boot/Image DTB=../cva6-zcu104-minimal.dtb TARGET=linux-rv64-cva6-zcu`


## 1.3) OpenSBI
> **_:notebook: Note:_** The following steps shall be executed using the *riscv64-linux-* toolchain.

First, clone **opensbi** and checkout to the **cva6** branch:

`git clone https://github.com/ninolomata/opensbi.git`\
`cd opensbi`\
`git checkout esrgv3/cva6`

Next, compile **opensbi** with the **baremetal application** or **linux** for the chosen target platform.

Examples:

To build **opensbi** with the **baremetal application** for fpga run:

`make PLATFORM=cva6-fpga-zcu FW_PAYLOAD=y FW_PAYLOAD_PATH=../baremetal-app/build/cva6-fpga-zcu/bare-metal_app.bin`

To build **opensbi** with **linux** for fpga run:

`make PLATFORM=cva6-fpga-zcu FW_PAYLOAD=y FW_PAYLOAD_PATH=../linux/lloader/linux-rv64-cva6-zcu.bin`

## 1.4) CVA6
> **_:notebook: Note:_**  Please make sure `RISCV` environment variable is set and proceed.

Clone the **cva6** repo:

`git clone https://github.com/openhwgroup/cva6.git`\
`cd cva6`\
`git checkout fbe1e01e`
`git submodule update --init --recursive`\

Setup cva6 according to the repo instructions.

Next, apply patches to **cva6** repo to include support for the zcu104:

`git apply ../patches/0001-add-initial-support-for-zcu104.patch`

# 2) FPGA

> **_:notebook: Note:_** Make sure that the RISCV enviroment variable is set before executing the next steps.

To build for fpga run:

`make fpga BOARD=zcu104`

## 2.1) Program the bitstream and run the code

First, in a separate terminal, connect to the UARTs using a program such as minicom:

`minicom -D /dev/ttyUSB1`

Next, run your target application as follow.

To run **opensbi** with the **baremetal app** or **linux**:

First, refer to section [OpenSBI](#13-opensbi) to compile **opensbi** with the proper application (**baremetal app** or **linux**). Then, run the following command, where `PAYLOAD` holds the path to the target binary file.

`make run BOARD=zcu104 PAYLOAD=/path/to/opensbi/build/platform/cva-fpga-zcu/firmware/fw_payload.bin`

To run a custom application:

`make run BOARD=zcu104 PAYLOAD=/path/to/application/file.bin`

When finish, you should see your application printing out information.

## Used tool versions

- riscv64-unknown-elf-gcc version 10.1.0 (SiFive GCC 8.3.0-2020.08.2)
- riscv64-linux-gcc version 9.3.0 (Buildroot 2020.02-00011-g7ea8a52) (for linux)
- Vivado 2020.2
- dtc 1.5.0
