#ifndef PLAT_H
#define PLAT_H

#define PLAT_MEM_BASE 0x80200000
#define PLAT_MEM_SIZE 0x00100000

#define PLAT_TIMER_FREQ (1000000ull) //1 MHz
#define UART_IRQ_ID (0)

#endif
