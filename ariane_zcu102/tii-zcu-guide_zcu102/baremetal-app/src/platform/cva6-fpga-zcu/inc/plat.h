#ifndef PLAT_H
#define PLAT_H

#define PLAT_MEM_BASE 0x40200000
#define PLAT_MEM_SIZE 0x08000000

#define PLAT_TIMER_FREQ (1000000ull) //1 MHz
#define UART_IRQ_ID (1)

#endif
