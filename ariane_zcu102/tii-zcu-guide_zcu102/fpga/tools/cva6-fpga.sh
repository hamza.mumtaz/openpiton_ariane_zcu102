#! /bin/bash

# $1 - platform (available zcu)
# $2 - path to bin file
set -e

FPGA_PATH=$(realpath ../fpga)

$FPGA_PATH/run $1 $2 0x40000000
