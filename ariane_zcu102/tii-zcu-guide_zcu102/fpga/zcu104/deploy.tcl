connect
targets -set -filter {name =~ "PSU"}

set path [file dirname [file normalize $argv0]]

source $path/hw/psu_init.tcl
psu_init
after 1000
psu_ps_pl_isolation_removal
after 1000
psu_ps_pl_reset_config

foreach {file addr} $::argv {
    puts "loading $file to $addr"
    dow -data $file $addr
}
set bit_file [glob -directory $path/hw/ *.bit]
puts "flashing bitstream"
fpga -f $bit_file

disconnect